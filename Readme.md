# Deploying Fuse components

  Use `odo` (OpenShift do) to deploy in OCP-4

  1) Package project in JAR file

      mvn package

  2) Prepare deployment 

      odo create java currency --binary target/currency-1.0.0.jar

  2) Push to OpenShift 

      odo push
  
  4) If you need to redeploy, first repackage then push, as follows
      
      mvn clean package
      
      odo push --source

</br>


# Register a new CamelK integration in the ServiceRegistry


  1) After the app starts, you can post new Integrations with curl:

```shell
      curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' 'http://localhost:8080/camel/services/register' -H 'service: {"name":"sample","description": "Sample service to show how to post register a new integration"}' -d "@src/main/resources/integrations/sample.xml"
```

  example:

```shell
      curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' 'http://serviceregistry-app-demo-bruno-02.apps.cluster-madrid-c0e3.madrid-c0e3.openshiftworkshop.com/camel/services/register' -H 'service: {"name":"sample","description": "Sample service to show how to register a new integration"}' -d "@newservice.xml"
```



</br>

## Testing the deployment

  Ensure there is a route exposing the `contentserver` service, then open a browser (tested with Chrome) and hit:

  http://{HOSTNAME}/camel/stream/currency/gpb-eur/{USERNAME}

  where `{HOSTNAME}` is the URL to the OCP route and `{USERNAME}` is a given user.

  This should open a stream and few moments later the stream should start pushing information into the browser like so:

```html
  Activating stream, please wait...
  1.1113
  1.1070
  1.1116
  1.1025
```
